package com.aboalata.base

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding

abstract class BaseAdapter<T>(callback: DiffUtil.ItemCallback<T>) :
    ListAdapter<T, BaseViewHolder<ViewBinding>>(callback) {

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding>, position: Int) {
        bind((holder as BaseViewHolder<*>).binding, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = getViewHolder(parent, viewType)

    open fun getViewHolder(parent: ViewGroup, viewType: Int) =
        BaseViewHolder(createBinding(parent, viewType))

    abstract fun createBinding(parent: ViewGroup, viewType: Int): ViewBinding

    protected abstract fun bind(binding: ViewBinding, position: Int)


}