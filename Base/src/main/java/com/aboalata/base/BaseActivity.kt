package com.aboalata.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View.inflate
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VB:ViewBinding>(val bindingFactory: (LayoutInflater) -> VB):AppCompatActivity() {

     val binding: VB by lazy { bindingFactory(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialize Binding
        setContentView(binding.root)

    }
}