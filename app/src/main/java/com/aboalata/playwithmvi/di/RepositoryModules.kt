package com.aboalata.playwithmvi.di

import com.aboalata.network.ApiService
import com.aboalata.network.MainRepository
import com.aboalata.network.MainRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModules {


//    @Binds
//    fun provideMainRepositoryImpl(repository: MainRepositoryImpl): MainRepository
//
//    @Binds
//     fun provideApiService(api: MainRepositoryImpl): ApiService
}