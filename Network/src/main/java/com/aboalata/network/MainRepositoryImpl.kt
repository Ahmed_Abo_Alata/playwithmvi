package com.aboalata.network

import com.aboalata.entites.Post
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepositoryImpl @Inject constructor( private val apiService: ApiService ):MainRepository {

    override suspend fun getPosts(): List<Post> {
        return apiService.getUsers()
    }
}