package com.aboalata.network

import com.aboalata.entites.Post
import retrofit2.http.GET

interface ApiService {

    @GET("posts")
    suspend fun getUsers(): List<Post>
}