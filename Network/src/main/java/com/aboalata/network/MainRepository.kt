package com.aboalata.network

import com.aboalata.entites.Post

interface MainRepository {
    suspend fun getPosts(): List<Post>
}