package com.aboalata.fetchpost.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aboalata.base.BaseViewHolder
import com.aboalata.entites.Post
import com.aboalata.fetchpost.R
import com.aboalata.fetchpost.databinding.ItemPostBinding

class PostAdapter( private val post: List<Post>,private val onClickListener: OnClickListener): RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {

        val itemBinding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)


        return PostViewHolder(itemBinding)
    }

    override fun getItemCount() = post.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {

        holder.binding.textViewUserName.text=post[position].title
        holder.binding.textViewUserEmail.text=post[position].body
        holder.binding.root.setOnClickListener {
            onClickListener.onClick(post = post[position])
        }

    }

    inner class PostViewHolder(binding: ItemPostBinding) :
        BaseViewHolder<ItemPostBinding>(binding)

    class OnClickListener(val clickListener: (post: Post) -> Unit) {
        fun onClick(post: Post) = clickListener(post)
    }
}