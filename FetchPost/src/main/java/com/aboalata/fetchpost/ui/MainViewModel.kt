package com.aboalata.fetchpost.ui

import androidx.lifecycle.viewModelScope
import com.aboalata.base.BaseViewModel
import com.aboalata.network.MainRepository
import com.aboalata.network.MainRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: MainRepositoryImpl) : BaseViewModel<MainContract.Event, MainContract.State, MainContract.Effect>(){

    /**
     * Create initial State of Views
     */
    override fun createInitialState(): MainContract.State {
        return MainContract.State(
            MainContract.PostState.Idle
        )
    }

    /**
     * Handle each event
     */
    override fun handleEvent(event: MainContract.Event) {
        when (event) {
            is MainContract.Event.OnFetchPost -> { fetchPost() }
            is MainContract.Event.OnDetailsScreenClicked -> {
                setEffect { MainContract.Effect.MoveToDetails }
            }
            is MainContract.Event.OnErrorMsg->{
                setEffect { MainContract.Effect.ShowErrorToast }
            }
        }
    }


    /**
     * Get Post Api
     */
    private fun fetchPost() {
        viewModelScope.launch {
            // Set Loading
            setState { copy(postState = MainContract.PostState.Loading) }
            try {

             val list=   mainRepository.getPosts()

                setState { copy(postState = MainContract.PostState.Success(Post = list)) }
            }
            catch (exception : Exception) {
                setEffect { MainContract.Effect.ShowErrorToast }
            }
        }
    }
}