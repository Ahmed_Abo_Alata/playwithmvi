package com.aboalata.fetchpost.ui

import com.aboalata.base.UiEffect
import com.aboalata.base.UiEvent
import com.aboalata.base.UiState
import com.aboalata.entites.Post


/**
 * Contract of [MainActivity]
 *
 * See if you want to another approach for create copy of sealed class
 *
 * https://ivanmorgillo.com/2020/10/28/how-to-fix-the-pain-of-modifying-kotlin-nested-data-classes/
 * https://ivanmorgillo.com/2020/11/19/doh-there-is-no-copy-method-for-sealed-classes-in-kotlin/
 */
class MainContract {

    sealed class Event : UiEvent {
        object OnFetchPost : Event()
        object OnDetailsScreenClicked : Event()
        object OnErrorMsg:Event()
    }

    data class State(
        val postState: PostState
    ) : UiState

    sealed class PostState {
        object Idle : PostState()
        object Loading : PostState()
        data class Success(val Post: List<Post>) : PostState()
    }

    sealed class Effect : UiEffect {

        object MoveToDetails : Effect()
        object ShowErrorToast:Effect()

    }
}