package com.aboalata.fetchpost.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.aboalata.base.BaseActivity
import com.aboalata.entites.Post
import com.aboalata.fetchpost.R
import com.aboalata.fetchpost.adapter.PostAdapter
import com.aboalata.fetchpost.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {


    private val viewModel : MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObservers()
        viewModel.setEvent(MainContract.Event.OnFetchPost)

    }


    /**
     * Initialize Observers
     */
    private fun initObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                when (it.postState) {
                    is MainContract.PostState.Idle -> { binding.progressBar.isVisible = false }
                    is MainContract.PostState.Loading -> { binding.progressBar.isVisible = true }
                    is MainContract.PostState.Success -> {
                        binding.progressBar.isVisible = false

                        showDataInRecycle(it.postState.Post)
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.effect.collect {
                when (it) {
                    is MainContract.Effect.MoveToDetails -> {
                        binding.progressBar.isVisible = false
                        // move to details
                    }
                    is MainContract.Effect.ShowErrorToast->{
                        binding.progressBar.isVisible = false
                        showToast("Something went Wrong")
                    }
                }
            }
        }
    }


    /**
     * Show simple toast message
     */
    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Move to screen Details
     */
    private fun moveToDetails(post: Post) {
        // handle move to details
        showToast("${post.body} ${post.title}")
    }

    /**
     * Show rc data
     */
    private fun showDataInRecycle( post: List<Post>)
    {
        binding.recpost.run {

            layoutManager=LinearLayoutManager(this@MainActivity,LinearLayoutManager.VERTICAL,false)
            adapter= PostAdapter(post,PostAdapter.OnClickListener{post -> moveToDetails(post) })
            (adapter as PostAdapter).notifyDataSetChanged()

        }
    }
}